﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class chatLogic : MonoBehaviour
{
    private Text chatText;
    public float showMessageTime;

    void Start ()
    {
        chatText = GetComponentInChildren<Text>();
    }

    public void SetMessage(string msg)
    {
        if(chatText.text != msg)
        {
            gameObject.SetActive(true);
            chatText.text = msg;
            StartCoroutine(HideMessage());
        }
    }

    IEnumerator HideMessage()
    {
        yield return new WaitForSeconds(showMessageTime);
        chatText.text = "";
        gameObject.SetActive(false);
    }
}
