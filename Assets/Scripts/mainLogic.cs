﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class mainLogic : MonoBehaviour

{
    public GameObject drinksMarker;
    public GameObject waiterMarker;
    public GameObject bierdeckelGhost;

    private GameObject currDrink;
    private GameObject currDrinkClone;
    private Transform currDrinkTrans;

    public mainNav mainNav;

    private Transform startMarker;
    private Transform endMarker;

    Animator m_Animator;

    // Movement speed in units/sec.
    public AnimationCurve MoveCurve;
    public float animationSpeed = 40F;

    public float switchRate = 1f;
    private float nextSwitch;

    public Vector3 targetScale = new Vector3(2.0f, 2.0f, 2.0f);

    // Time when the movement started.
    private float startTime;

    // Total distance between the markers.
    private float journeyLength;


    public float threshholdDistance = 60.0f;
    public float threshholdAngleZ = 30.0f;
    public float threshholdAngleX = 30.0f;
    private float deltaAngleZ;
    private float deltaAngleX;

    private float markerDist;
    private float drinksAngle;

    private bool welcomeBubbleAcitve = false;
    private bool startDrinkSlidingAnim = false;
    private bool markersClose = false;

    private GameObject drinks;

    public GameObject chat;

    public GameObject arrowX;
    public GameObject arrowZ;

    // Start is called before the first frame update
    void Start()
    {
        drinks = GameObject.Find("drinks");
        drinks.SetActive(false);

        mainNav = GetComponent<mainNav>();
        Debug.Log(mainNav);

        m_Animator = waiterMarker.GetComponentInChildren<Animator>();
    }

    // Update is called once per frame
    void Update()
    {

        if (startDrinkSlidingAnim)
        {
            // Distance moved = time * speed.
            float distCovered = (Time.time - startTime) * animationSpeed;

            // Fraction of journey completed = current distance divided by total distance.
            float fracJourney = distCovered / journeyLength;

            // Set our position as a fraction of the distance between the markers.
            if(currDrinkClone != null)
            {
                currDrinkClone.transform.position = Vector3.Lerp(startMarker.position, endMarker.position, MoveCurve.Evaluate(fracJourney));
                currDrinkClone.transform.localScale = Vector3.Lerp(new Vector3(5.0f,5.0f,5.0f), targetScale, MoveCurve.Evaluate(fracJourney));
            }

            if(fracJourney >= 1 && currDrinkClone != null)
            {
                Destroy(currDrinkClone);
                startDrinkSlidingAnim = false;
            }
        }

        markerDist = Vector3.Distance(waiterMarker.transform.position, drinksMarker.transform.position);

        if (markerDist < threshholdDistance && markerDist != 0)
        {
            drinks.SetActive(true);
            markersClose = true;
            bierdeckelGhost.SetActive(false);
            if (!welcomeBubbleAcitve)
            {
                welcomeBubbleAcitve = true;
                ShowInstructions();
            }
        }
        

        deltaAngleZ = Mathf.Abs(Mathf.DeltaAngle(drinksMarker.transform.localEulerAngles.z, waiterMarker.transform.localEulerAngles.z));

        if (threshholdAngleZ < deltaAngleZ && markersClose && !startDrinkSlidingAnim)
        {
            startDrinkSlidingAnim = true;
            currDrink = GameObject.FindGameObjectWithTag("activeDrink");
            startMarker = currDrink.transform;
            currDrinkClone = Instantiate(currDrink, currDrink.transform.position, currDrink.transform.rotation);
            currDrinkClone.tag = "Untagged";
            currDrinkClone.transform.SetParent(drinks.transform, false);
            endMarker = waiterMarker.GetComponentInChildren<Transform>();
            startTime = Time.time;
            journeyLength = Vector3.Distance(startMarker.position, endMarker.position);

            m_Animator.SetTrigger("order");
            chat.transform.GetComponent<chatLogic>().SetMessage( "thanks for ordering a " + currDrink.name + ". We will bring it right away!");
        }

        if (markerDist > threshholdDistance || markerDist == 0)
        {
            welcomeBubbleAcitve = false;
            markersClose = false;
            bierdeckelGhost.SetActive(true);
            if(drinks != null)
            {
                drinks.SetActive(false);
            }
        }

        deltaAngleX = Mathf.DeltaAngle(drinksMarker.transform.localEulerAngles.x, waiterMarker.transform.localEulerAngles.x);


        if (markersClose && !startDrinkSlidingAnim)
        {
            bool isBigger = deltaAngleX > threshholdAngleX;
            if (deltaAngleX > threshholdAngleX && Time.time > nextSwitch)
            {
                nextSwitch = Time.time + switchRate;
                mainNav.SwitchDrink(true);
            }
            else if(deltaAngleX < threshholdAngleX * -1 && Time.time > nextSwitch)
            {
                nextSwitch = Time.time + switchRate;
                mainNav.SwitchDrink(false);
            }
        }

    }

    public void ShowInstructions()
    {
        arrowX.GetComponent<arrowTween>().HideArrowTimeout();
        arrowX.SetActive(true);
        chat.transform.GetComponent<chatLogic>().SetMessage("flip cocktail card on X Axis (width) for changing the drink.");
        StartCoroutine(FlipVerticalMessage());
    }

    IEnumerator FlipVerticalMessage()
    {
        arrowZ.GetComponent<arrowTween>().HideArrowTimeout();
        yield return new WaitForSeconds(3);
        arrowZ.SetActive(true);
        chat.transform.GetComponent<chatLogic>().SetMessage("flip cocktail card Z Axis (depth) for ordering the current drink.");
    }
}
