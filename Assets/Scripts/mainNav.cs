﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class mainNav : MonoBehaviour {

    public Button btn_next, btn_prev;
    public GameObject drinksObject;
    private Transform[] drinksArray;
    private Transform[] finalDrinksArray;
    private int currDrinkIndex;
    private int index;

    public GameObject chat;

    // Use this for initialization
    void Start ()
    {
        currDrinkIndex = 0;
        finalDrinksArray = new Transform[drinksObject.transform.childCount];
        index = 0;
        foreach( Transform drink in drinksObject.transform)
        {
            // Debug.Log("drink.name: " + drink.name);
            if(drinksObject.transform != drink)
            {
                finalDrinksArray[index] = drink;
                index++;
            }
        }
        btn_next.onClick.AddListener(delegate { SwitchDrink(true); });
        btn_prev.onClick.AddListener(delegate { SwitchDrink(false); });
    }

    public void SwitchDrink(bool up)
    {
        //Debug.Log("Up? " + up); 
        //Debug.Log("Number of Drinks: " + drinksArray.Length);
        if (up)
        {
            currDrinkIndex++;
        }
        else
        {
            currDrinkIndex--;
        }
        if(currDrinkIndex >= finalDrinksArray.Length)
        {
            currDrinkIndex = 0;
        }
        if(currDrinkIndex < 0)
        {
            currDrinkIndex = finalDrinksArray.Length - 1;
        }
        foreach ( Transform drink in finalDrinksArray)
        {
            drink.gameObject.SetActive(false);
            drink.gameObject.tag = "Untagged";
        }

        Debug.Log("index: " + currDrinkIndex + " name: " + finalDrinksArray[currDrinkIndex].gameObject.name);
        finalDrinksArray[currDrinkIndex].gameObject.SetActive(true);
        finalDrinksArray[currDrinkIndex].gameObject.tag = "activeDrink";
    }
	
	// Update is called once per frame
	void Update () {
		
	}
}
