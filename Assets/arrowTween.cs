﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class arrowTween : MonoBehaviour
{

    public float timeToLive = 2f;
    public float timeout = 0.3f;
    public Transform startMarker;
    public Transform endMarker;

    public Color startColor;
    public Color endColor;

    public AnimationCurve MoveCurve;
    public AnimationCurve ColorCourve;

    // Movement speed in units/sec.
    public float speed = 60.0F;

    public float timeToShow = 3f;

    // Time when the movement started.
    private float startTime;

    // Total distance between the markers.
    private float journeyLength;

    Renderer rend;

    // Start is called before the first frame update
    void Start()
    {

        startTime = Time.time;
        journeyLength = Vector3.Distance(startMarker.position, endMarker.position);
        rend = GetComponent<Renderer>();
        StartCoroutine(HideArrow());
    }

    // Update is called once per frame
    void Update()
    {
        // Distance moved = time * speed.
        float distCovered = (Time.time - startTime) * speed;

        // Fraction of journey completed = current distance divided by total distance.
        float fracJourney = distCovered / journeyLength;
        float fracJourneyColor = fracJourney * 5;

        // Set our position as a fraction of the distance between the markers.
        if(fracJourney < 1)
        {
            transform.position = Vector3.Lerp(startMarker.position, endMarker.position, MoveCurve.Evaluate(fracJourney));
            transform.rotation = Quaternion.Lerp(startMarker.rotation, endMarker.rotation, fracJourney);

            if(fracJourney < 0.2)
            {            
                rend.material.color = Color.Lerp(startColor, endColor, ColorCourve.Evaluate(fracJourneyColor));
            }
            else if(fracJourney > 0.8)
            {
                rend.material.color = Color.Lerp(endColor, startColor, ColorCourve.Evaluate(fracJourney));
            }
        }
        else
        {
            StartCoroutine(ResetPosition());
        }
    }

    public void HideArrowTimeout()
    {
        StartCoroutine(ResetPosition());
    }

    IEnumerator ResetPosition()
    {
        transform.position = startMarker.position;
        yield return new WaitForSeconds(timeout);
        startTime = Time.time;
    }

    IEnumerator HideArrow()
    {
        yield return new WaitForSeconds(timeToShow);
        gameObject.SetActive(false);
    }
}
